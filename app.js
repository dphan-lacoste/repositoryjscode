const express = require('express')
const app = express()

app.get('/', function(req, res) {
  res.setHeader('Content-Type', 'text/plain');
  res.send('Vous êtes à l\'accueil, que puis-je pour vous ?');
});

app.get('/sous-sol', function(req, res) {
  res.setHeader('Content-Type', 'text/plain');
  res.send('Vous êtes dans la cave à vins, ces bouteilles sont à moi !');
});

app.get('/etage/1/chambre', function(req, res) {
  res.setHeader('Content-Type', 'text/plain');
  res.send('Hé ho, c\'est privé ici !');
});

app.get('/etage/1/chambre2', function(req, res) {
  res.setHeader('Content-Type', 'text/plain');
  res.send('Hé ho, c\'est privé ici !');
});

app.get('/etage/1/chambre3', function(req, res) {
  res.setHeader('Content-Type', 'text/plain');
  res.send('Hé ho, c\'est privé ici !');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})